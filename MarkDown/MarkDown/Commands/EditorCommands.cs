﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MarkDown.Commands
{
    public static class EditorCommands
    {
        public static readonly RoutedUICommand Headered = new RoutedUICommand
            (
                "Headered",
                "Headered",
                typeof(EditorCommands),
                new InputGestureCollection()
                {
                    new KeyGesture(Key.H, ModifierKeys.Control)
                }
            );

        public static readonly RoutedUICommand Unheadered = new RoutedUICommand
            (
                "Unheadered",
                "Unheadered",
                typeof(EditorCommands),
                new InputGestureCollection()
                {
                    new KeyGesture(Key.H, ModifierKeys.Control | ModifierKeys.Shift)
                }
            );

        public static readonly RoutedUICommand Strikethrough = new RoutedUICommand
            (
                "Strikethrough",
                "Strikethrough",
                typeof(EditorCommands),
                new InputGestureCollection()
                {
                    new KeyGesture(Key.T, ModifierKeys.Control)
                }
            );

        public static readonly RoutedUICommand Unstrike = new RoutedUICommand
            (
                "Unstrike",
                "Unstrike",
                typeof(EditorCommands),
                new InputGestureCollection()
                {
                    new KeyGesture(Key.T, ModifierKeys.Control | ModifierKeys.Shift)
                }
            );

        public static readonly RoutedUICommand CopyHtml = new RoutedUICommand
            (
                "CopyHtml",
                "CopyHtml",
                typeof(EditorCommands),
                new InputGestureCollection()
                {
                    new KeyGesture(Key.C, ModifierKeys.Control | ModifierKeys.Alt)
                }
            );
    }
}

﻿using System;
using System.Globalization;
using System.Windows.Data;
using CommonMark;

namespace MarkDown.Converters
{
    public class MarkdownConverter : IValueConverter
    {
        private CommonMarkSettings settings;

        public MarkdownConverter()
        {
            settings = CommonMarkSettings.Default.Clone();

            settings.AdditionalFeatures = CommonMarkAdditionalFeatures.All;
            settings.OutputFormat = OutputFormat.Html;
            settings.RenderSoftLineBreaksAsLineBreaks = true;
            settings.TrackSourcePosition = true;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return CommonMark.CommonMarkConverter.Convert(value as string, settings);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

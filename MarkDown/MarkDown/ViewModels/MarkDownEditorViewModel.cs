﻿using MarkDown.Models;
using Microsoft.Win32;
using System.IO;
using System.Windows.Controls;
using System.Windows.Input;

namespace MarkDown.ViewModels
{
    public class MarkDownEditorViewModel : ObservableObject
    {
        private MarkDownEditor editor = new MarkDownEditor();

        public string Markdown
        {
            get { return editor.Markdown; }
            set
            {
                if (editor.Markdown != value)
                {
                    editor.Markdown = value;
                    NotifyPropertyChanged(nameof(Markdown));
                }
            }
        }

        public string CurrentFile
        {
            get { return editor.CurrentFile; }
            set { editor.CurrentFile = value; }
        }
    }
}

﻿using System.Windows;
using MarkDown.ViewModels;
using Microsoft.Win32;
using System.Windows.Input;
using System.IO;
using System.Windows.Controls;

namespace MarkDown
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MarkDownEditorViewModel Editor { get; set; } = new MarkDownEditorViewModel();
        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = Editor;
        }

        #region Commands Methods
        private void AboutExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("It's a simple markdown editor", "About");
        }

        private void NewFileExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "MarkDown file (*.md)|*.md|Text file (*.txt)|*.txt";
            saveDialog.Title = "New File";
            saveDialog.AddExtension = true;
            if (saveDialog.ShowDialog() == true)
            {
                File.Create(saveDialog.FileName).Close();
                Editor.Markdown = "";
                Editor.CurrentFile = saveDialog.FileName;
            }
        }

        private void OpenFileExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Markdown files (*.md)|*.md|Text files (*.txt)|*.txt";
            if (openDialog.ShowDialog() == true)
            {
                Editor.Markdown = File.ReadAllText(openDialog.FileName);
                Editor.CurrentFile = openDialog.FileName;
            }
        }

        private void SaveAsExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "MarkDown file (*.md)|*.md|Text file (*.txt)|*.txt";
            if (saveDialog.ShowDialog() == true)
            {
                File.WriteAllText(saveDialog.FileName, Editor.Markdown);
                Editor.CurrentFile = saveDialog.FileName;
            }
        }

        private void SaveExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            File.WriteAllText(Editor.CurrentFile, Editor.Markdown);
        }

        private void SaveCanExecuted(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Editor.CurrentFile != null;
        }

        private void HeaderedExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var start = textBox.SelectionStart;

            string insertion = string.Empty;
            
            var lineIndex = textBox.GetLineIndexFromCharacterIndex(textBox.SelectionStart);
            var characterIndex = textBox.GetCharacterIndexFromLineIndex(lineIndex);

            if (Editor.Markdown.Length > 0 && Editor.Markdown[characterIndex] == '#')
            {
                insertion = "#";
            }
            else
            {
                insertion = "# ";
            }

            Editor.Markdown = Editor.Markdown.Insert(characterIndex, insertion);
            textBox.SelectionStart = start + insertion.Length;
        }

        private void HeaderedCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var lineIndex = textBox.GetLineIndexFromCharacterIndex(textBox.SelectionStart);
            var line = textBox.GetLineText(lineIndex);
            
            bool canExecute = false;

            if (line.Length < 6)
            {
                canExecute = true;
            }
            else
            {
                for (int i = 0; i < line.Length && i < 6; ++i)
                {
                    if (line[i] != '#')
                    {
                        canExecute = true;
                    }
                }
            }

            e.CanExecute = canExecute;
        }

        private void UnheaderedExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var start = textBox.SelectionStart;

            var lineIndex = textBox.GetLineIndexFromCharacterIndex(textBox.SelectionStart);
            var characterIndex = textBox.GetCharacterIndexFromLineIndex(lineIndex);

            int removedCount = 0;

            if (Editor.Markdown.Length > 0 && Editor.Markdown[characterIndex] == '#' && Editor.Markdown[characterIndex + 1] == ' ')
            {
                removedCount = 2;
            }
            else
            {
                removedCount = 1;
            }

            Editor.Markdown = Editor.Markdown.Remove(characterIndex, removedCount);
            textBox.SelectionStart = start - removedCount;
        }

        private void UnheaderedCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var lineIndex = textBox.GetLineIndexFromCharacterIndex(textBox.SelectionStart);
            var line = textBox.GetLineText(lineIndex);

            if (line.Length >= 2 && line[0] == '#' && (line[1] == '#' || line[1] == ' '))
            {
                e.CanExecute = true;
            }
            else
            {
                e.CanExecute = false;
            }
        }

        private void CopyHtmlExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Clipboard.SetText(webBrowser.Html);
        }

        private void CopyHtmlCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !string.IsNullOrWhiteSpace(webBrowser.Html);
        }

        private void StrikethroughExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var selectionLength = textBox.SelectionLength;
            var start = textBox.SelectionStart;
            var end = start + selectionLength;

            Editor.Markdown = Editor.Markdown.Insert(end, "~~");
            Editor.Markdown = Editor.Markdown.Insert(start, "~~");

            textBox.SelectionStart = start + 2;
            textBox.SelectionLength = selectionLength;
        }

        private void StrikethroughCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var start = textBox.SelectionStart;
            var end = start + textBox.SelectionLength;
            bool cantExecute = true;

            for (int i = 1; i <= 2; ++i)
            {
                if (start - i >= 0 && Editor.Markdown[start - i] != '~')
                {
                    cantExecute = false;
                }
                if (end - 1 + i < Editor.Markdown.Length && Editor.Markdown[end - 1 + i] != '~')
                {
                    cantExecute = false;
                }
            }
            if (start - 2 < 0 || end + 1 >= Editor.Markdown.Length)
            {
                cantExecute = false;
            }
            if (start == end)
            {
                cantExecute = true;
            }

            e.CanExecute = !cantExecute;
        }

        private void UnstrikeExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var selectionLength = textBox.SelectionLength;
            var start = textBox.SelectionStart;
            var end = start + selectionLength;

            Editor.Markdown = Editor.Markdown.Remove(end, 2);
            Editor.Markdown = Editor.Markdown.Remove(start-2, 2);

            textBox.SelectionStart = start - 2;
            textBox.SelectionLength = selectionLength;
        }

        private void UnstrikeCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var start = textBox.SelectionStart;
            var end = start + textBox.SelectionLength;
            bool canExecute = true;

            for (int i = 1; i <= 2; ++i)
            {
                if (start - i >= 0 && Editor.Markdown[start - i] != '~')
                {
                    canExecute = false;
                }
                if (end - 1 + i < Editor.Markdown.Length && Editor.Markdown[end - 1 + i] != '~')
                {
                    canExecute = false;
                }
            }
            if (start - 2 < 0 || end + 1 >= Editor.Markdown.Length)
            {
                canExecute = false;
            }
            if (start == end)
            {
                canExecute = false;
            }

            e.CanExecute = canExecute;
        }
        #endregion
    }
}

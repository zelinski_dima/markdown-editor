﻿namespace MarkDown.Models
{
    public class MarkDownEditor
    {
        public string Markdown { get; set; } = "";
        public string CurrentFile { get; set; } = null;
    }
}

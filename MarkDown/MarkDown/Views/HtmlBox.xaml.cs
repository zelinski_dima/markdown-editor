﻿using System.Windows;
using System.Windows.Controls;

namespace MarkDown.Views
{
    /// <summary>
    /// Interaction logic for HtmlBox.xaml
    /// </summary>
    public partial class HtmlBox : UserControl
    {
        public HtmlBox()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty HtmlProperty = DependencyProperty.Register("Html", typeof(string), typeof(HtmlBox));

        public string Html
        {
            get { return (string)GetValue(HtmlProperty); }
            set { SetValue(HtmlProperty, value); }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == HtmlProperty)
            {
                DoBrowse();
            }
        }
        private void DoBrowse()
        {
            if (Html != null)
            {
                browser.NavigateToString(Html);
            }
        }
    }
}
